""" open ptyxis plugin for nautilus

Turtle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Turtle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Turtle. If not, see <https://www.gnu.org/licenses/>. 
"""
import os
import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Nautilus, GObject


class OpenPtyxisMenuProvider(GObject.GObject, Nautilus.MenuProvider):
    """ open ptyxis provider class for nautilus """

    def _open_ptyxis(self, _menu, path):
        os.system(
            "/usr/bin/flatpak run app.devsuite.Ptyxis "
            f"--new-window --working-directory={path} &")

    def _create_menu_item(self, path):
        item = Nautilus.MenuItem(
            name="OpenPtyxis::open",
            label="Open Ptyxis",
            tip="",
            icon="")
        item.connect('activate', self._open_ptyxis, path)

        return item

    def get_background_items(self, file):
        """ get_background_items from Nautilus.MenuProvider """
        path = file.get_location().get_path()
        return [self._create_menu_item(path)]

    def get_file_items(self, files):
        """ get_file_items from Nautilus.MenuProvider """
        if len(files) == 1 and files[0].is_directory():
            path = files[0].get_location().get_path()
            return [self._create_menu_item(path)]

        return []
